# Coverage Api

Contains a flask application which contains a route where a address give the coverage of the city of the address.

## launch

```bash
# On the coverage_api folder
python -m flask run
```

## routes

`/` where `?q` must contains the address of the location to determine coverage.

## example

`your_api/?q=42+rue+papernest+75011+Paris`

## result 

```json
{"Bouygues Telecom":{"2G":true,"3G":true,"4G":true},"Free mobile":{"2G":false,"3G":true,"4G":true},"Orange":{"2G":true,"3G":true,"4G":true},"SFR":{"2G":true,"3G":true,"4G":true},"city":{"name":"Lorient","postcode":"56100"}}
```