import requests
import sqlite3
from typing import Tuple, List, Dict

from flask import Flask, request, abort
from flask import g

app = Flask(__name__)
DATABASE = 'database.db'


@app.route('/')
def get_coverage_from_query():
    if "q" not in request.args:
        abort(401)
    city_name, postcode = get_city_from_query(request.args["q"])
    if city_name == "Unknown":
        abort(500, "Vile non trouvée avec la query " + request["q"])
    id_city: str = query_db("select ID_CITY FROM CITY WHERE NAME=? AND POSTAL=?", (city_name, postcode), one=True)
    if id_city is None:
        abort(500, str.format("Vile : {}, {} non trouvé dans la base de donnée ", city_name, postcode))
    coverage_for_city: List[Tuple[str, str, str, str]] = query_db(
        "select ID_OPERATOR, IS_2G_COVER, IS_3G_COVER, IS_4G_COVER FROM OPERATOR_COVERAGE WHERE ID_CITY=?",
        id_city)
    result: Dict = {"city": {"name": city_name, "postcode": postcode}}
    for coverage in coverage_for_city:
        operator_name = \
            query_db("select NAME FROM OPERATOR WHERE ID_OPERATOR=?", str(coverage[0]), one=True)[0]
        result[operator_name] = {"2G": coverage[1] == 1, "3G": coverage[2] == 1, "4G": coverage[3] == 1}
    return result


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def get_city_from_query(address: str) -> Tuple[str, str]:
    try:
        adress_api_request = requests.get(
            str.format('https://api-adresse.data.gouv.fr/search/?q={}', address))
        property_first_req = adress_api_request.json()["features"][0]["properties"]
        return property_first_req["city"], property_first_req["postcode"]

    except IndexError:
        return "Unknown", "Unknown"


if __name__ == '__main__':
    app.run()
