import csv
from io import TextIOWrapper
from typing import Dict, Union, OrderedDict, List
from model.model import Position, Operator, OperatorCoverage
from model.process import CsvCreatorProcess
from pathlib import Path
import argparse


def read_csv_and_return_dict(file_path: TextIOWrapper) -> List[Union[Dict[str, str], OrderedDict[str, str]]]:
    reader = csv.DictReader(file_path, delimiter=";")
    return [row for row in reader]


def create_dict_operator_by_mnc_from_data(data: List[Union[Dict[str, str], OrderedDict[str, str]]]) -> Dict[
    int, Operator]:
    operators_by_mnc = {}

    for data_operator in data:
        operator = Operator(
            mnc=Operator.get_mnc_from_mcc_and_mnc3(int(data_operator["MCC"]), int(data_operator["MNC3"])),
            name=data_operator["Operateur"])
        operators_by_mnc[int(operator.mnc)] = operator

    return operators_by_mnc


def create_csv_line_for_operateur_location(data: Union[Dict[str, str], OrderedDict[str, str]]) -> str:
    lon, lat = Position.convert_lambert_to_gps(int(data["X"]), int(data["Y"]))
    print(";".join([str(lon), str(lat), data["Operateur"]]))
    return ";".join([str(lon), str(lat), data["Operateur"]])


def launch_multi_process(data_coverage_mnc: List[Union[Dict[str, str], OrderedDict[str, str]]],
                         dict_operator_by_mnc: Dict[int, Operator],
                         number_of_thread: int = 1,
                         result_dir: str = "../resources/"):
    size_between_process = len(data_coverage_mnc) / number_of_thread
    processes: List[CsvCreatorProcess] = []

    result_content: str = OperatorCoverage.header_csv() + "\n"
    error_content: str = OperatorCoverage.header_error_csv() + "\n"

    for i in range(1, (number_of_thread + 1)):
        start_index = int(size_between_process * (i - 1))
        end_index = int(size_between_process * i)
        process = CsvCreatorProcess(list_data=data_coverage_mnc[start_index:end_index],
                                    operator_by_mnc=dict_operator_by_mnc,
                                    result_path=str.format(result_dir + "/result_{}.csv", i),
                                    error_path=str.format(result_dir + "/error_{}.csv", i))
        process.daemon = True
        process.start()
        processes.append(process)
        print(str.format("Launch process {} with data from {} to {}", i, start_index, end_index))

    print("Waiting process")
    for process in processes:
        process.join()

        result_file = Path(process.result_path)
        error_file = Path(process.error_path)

        result_content += result_file.read_text() + "\n"
        error_content += error_file.read_text() + "\n"

        result_file.unlink()
        error_file.unlink()

    print("All process finished writing files")

    Path(result_dir + "/result.csv").write_text(result_content)
    Path(result_dir + "/error.csv").write_text(error_content)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data_coverage_mnc", help="Path of the file which contains the link between MNC and Coverage",
                        type=argparse.FileType('r'))
    parser.add_argument("data_mnc_operator",
                        help="Path of the file which contains the link between MNC and Operator name",
                        type=argparse.FileType('r'))
    parser.add_argument("--number-of-process", help="Number of process to launch used to launch the parsing", type=int,
                        default=1)
    parser.add_argument("--result-dir", help="Directory to store result.csv and error.csv", type=str,
                        default="../resources/")

    args = parser.parse_args()

    coverage_mnc: List[Union[Dict[str, str], OrderedDict[str, str]]] = read_csv_and_return_dict(
        args.data_coverage_mnc)
    mnc_operator: List[Union[Dict[str, str], OrderedDict[str, str]]] = read_csv_and_return_dict(
        args.data_mnc_operator)

    operator_by_mnc: Dict[int, Operator] = create_dict_operator_by_mnc_from_data(mnc_operator)

    launch_multi_process(data_coverage_mnc=coverage_mnc,
                         dict_operator_by_mnc=operator_by_mnc,
                         number_of_thread=args.number_of_process,
                         result_dir=args.result_dir)
