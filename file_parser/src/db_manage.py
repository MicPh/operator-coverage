import sqlite3
import argparse
from io import TextIOWrapper
import csv
from model import Operator, OperatorCoverage, City, fuse_operator_coverage_in_one
from typing import Tuple
import itertools


def create_db(cursor_conn: sqlite3.Cursor) -> None:
    cursor_conn.executescript(
        """
        CREATE TABLE CITY(
            ID_CITY INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
            NAME TEXT NOT NULL,
            POSTAL INTEGER
        );

        CREATE TABLE OPERATOR(
            ID_OPERATOR INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
            NAME TEXT NOT NULL,
            MNC INTEGER
        );

        CREATE TABLE OPERATOR_COVERAGE(
            ID_OPERATOR_COVERAGE INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
            ID_OPERATOR INTEGER REFERENCES OPERATOR NOT NULL,
            ID_CITY INTEGER REFERENCES CITY NOT NULL,
            IS_2G_COVER INTEGER DEFAULT 0,
            IS_3G_COVER INTEGER DEFAULT 0,
            IS_4G_COVER INTEGER DEFAULT 0
        );
        """
    )


def drop_db(cursor_conn: sqlite3.Cursor) -> None:
    cursor_conn.executescript(
        """
        DROP TABLE IF EXISTS CITY;

        DROP TABLE IF EXISTS OPERATOR;

        DROP TABLE IF EXISTS OPERATOR_COVERAGE;
        """
    )


def get_or_insert_city(cursor_conn: sqlite3.Cursor, city: City) -> Tuple[str, str, str]:
    cursor_conn.execute("SELECT * FROM CITY where NAME=? AND POSTAL=?", (city.name, city.postcode))
    city_db = cursor_conn.fetchone()
    if not city_db:
        cursor_conn.execute("INSERT INTO CITY(NAME,POSTAL) VALUES (?,?)", (city.name, city.postcode))
        return get_or_insert_city(cursor_conn, city)
    return city_db


def get_or_insert_operator(cursor_conn: sqlite3.Cursor, operator: Operator) -> Tuple[str, str, str]:
    cursor_conn.execute("SELECT * FROM OPERATOR where NAME=? AND MNC=?", (operator.name, operator.mnc))
    operator_db = cursor_conn.fetchone()
    if not operator_db:
        cursor_conn.execute("INSERT INTO OPERATOR(NAME,MNC) VALUES (?, ?)", (operator.name, operator.mnc))
        return get_or_insert_operator(cursor_conn, operator)
    return operator_db


def update_or_insert_operator_coverage(cursor_conn: sqlite3.Cursor, city_id: str, operator_id: str,
                                       coverage: OperatorCoverage):
    cursor_conn.execute("SELECT * FROM OPERATOR_COVERAGE where ID_CITY=? AND ID_OPERATOR=?", (city_id, operator_id))
    result_coverage = cursor_conn.fetchone()
    if not result_coverage:
        # We must insert the data
        cursor_conn.execute(
            "INSERT INTO OPERATOR_COVERAGE(ID_CITY, ID_OPERATOR, IS_2G_COVER, IS_3G_COVER, IS_4G_COVER) VALUES (?, ?, ?, ?, ?)",
            (city_id, operator_id, coverage.is_2g_cover_int, coverage.is_3g_cover_int, coverage.is_4g_cover_int))
    else:
        prev_coverage = OperatorCoverage(city=coverage.city, operator=coverage.operator,
                                         is_2g_cover=result_coverage[3] == 1,
                                         is_3g_cover=result_coverage[4] == 1,
                                         is_4g_cover=result_coverage[5] == 1)
        new_coverage = fuse_operator_coverage_in_one(prev_coverage, coverage)
        # We must update the data
        cursor_conn.execute(
            "UPDATE OPERATOR_COVERAGE SET IS_2G_COVER=?, IS_3G_COVER=?, IS_4G_COVER=? WHERE ID_OPERATOR_COVERAGE=?",
            (new_coverage.is_2g_cover, new_coverage.is_3g_cover, new_coverage.is_4g_cover,
             result_coverage[0])
        )


def update_db(cursor_conn: sqlite3.Cursor, file: TextIOWrapper):
    dict_reader = csv.DictReader(file, delimiter=";")

    rows = [row for row in dict_reader]

    operator_coverage = [
        OperatorCoverage(operator=Operator(name=row["Operator"], mnc=int(row["MNC"])),
                         city=City(name=row["City"], postcode=row["Postcode"]),
                         is_2g_cover=row["2G"] == "True",
                         is_3g_cover=row["3G"] == "True",
                         is_4g_cover=row["4G"] == "True")
        for row in rows
    ]

    iterator_operator_coverage_by_city_and_operator = itertools.groupby(operator_coverage,
                                                                        lambda x: (x.city, x.operator))

    for key, group in iterator_operator_coverage_by_city_and_operator:
        city: Tuple[str, str, str] = get_or_insert_city(cursor_conn=cursor_conn, city=key[0])
        operator: Tuple[str, str, str] = get_or_insert_operator(cursor_conn=cursor_conn, operator=key[1])

        result_coverage = list(itertools.accumulate(group, lambda x, y: fuse_operator_coverage_in_one(x, y)))[0]
        update_or_insert_operator_coverage(cursor_conn=cursor_conn, city_id=city[0], operator_id=operator[0],
                                           coverage=result_coverage)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="path for the file used for update", type=argparse.FileType('r'))
    parser.add_argument("--create", help="activate to create the database with empty structure", action="store_true")
    parser.add_argument("--drop", help="activate to drop the existing tables of the database", action="store_true")
    parser.add_argument("--name-database", help="name of the database", type=str, default="database.db")

    args = parser.parse_args()

    conn = sqlite3.connect(args.name_database)
    c = conn.cursor()

    if args.drop:
        drop_db(c)

    if args.create:
        create_db(c)

    update_db(c, args.file)

    conn.commit()

    conn.close()
