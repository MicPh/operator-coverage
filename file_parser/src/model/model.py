from typing import Tuple

import pyproj
import requests


class Position(object):

    def __init__(self, lat: float, lon: float) -> None:
        self.__lat = lat
        self.__lon = lon

    def __hash__(self):
        return hash((self.__lat, self.__lon))

    def __eq__(self, other):
        if not isinstance(other, type(self)): return NotImplemented
        return self.__lat == other.__lat and self.__lon == other.__lon

    @property
    def lat(self) -> float:
        return self.__lat

    @property
    def lon(self) -> float:
        return self.__lon

    @staticmethod
    def convert_lambert_to_gps(x: int, y: int) -> Tuple[float, float]:
        lambert = pyproj.Proj(
            '+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs')
        wgs84 = pyproj.Proj('+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs')
        return pyproj.transform(lambert, wgs84, x, y)

    def __repr__(self):
        return str.format("lat:{},lon:{}", self.__lat, self.__lon)


class City:

    def __init__(self, name: str, postcode: str) -> None:
        self.__name: str = name
        self.__postcode: str = postcode

    def __hash__(self):
        return hash((self.__name, self.__postcode))

    def __eq__(self, other):
        if not isinstance(other, type(self)): return NotImplemented
        return self.__name == other.__name and self.__postcode == other.__postcode

    @property
    def name(self):
        return self.__name

    @property
    def postcode(self):
        return self.__postcode

    @staticmethod
    def get_city_from_position(pos: Position) -> Tuple[str, str]:
        try:
            adress_api_request = requests.get(
                str.format('https://api-adresse.data.gouv.fr/reverse/?lat={}&lon={}', pos.lat, pos.lon))
            property_first_req = adress_api_request.json()["features"][0]["properties"]
            return property_first_req["city"], property_first_req["postcode"]

        except IndexError:
            return "Unknown", "Unknown"

    def __repr__(self):
        return str.format("name:{},postcode:{}", self.__name, self.__postcode)


def get_city_from_lambert_coord(x: int, y: int) -> City:
    lon, lat = Position.convert_lambert_to_gps(x, y)
    name, postcode = City.get_city_from_position(Position(lat=lat, lon=lon))
    if name == "Unknown":
        return City("Error with X = " + str(x), "Y = " + str(y))
    return City(name, postcode)


class Operator:

    def __init__(self, name: str, mnc: int = None) -> None:
        self.__name: str = name
        self.__mnc: int = mnc

    def __hash__(self):
        return hash((self.__name, self.__mnc))

    def __eq__(self, other):
        if not isinstance(other, type(self)): return NotImplemented
        return self.__name == other.__name and self.__mnc == other.__mnc

    @property
    def name(self):
        return self.__name

    @property
    def mnc(self):
        return self.__mnc

    @staticmethod
    def get_mnc_from_mcc_and_mnc3(mcc: int, mnc3: int) -> int:
        return mcc * 100 + mnc3

    def __repr__(self):
        return str.format("name:{},mnc:{}", self.__name, self.__mnc)


class OperatorCoverage:

    def __init__(self, operator: Operator, city: City, is_2g_cover: bool = False, is_3g_cover: bool = False,
                 is_4g_cover: bool = False) -> None:
        self.__operator: Operator = operator
        self.__city: City = city

        self.__is_2g_cover: bool = is_2g_cover
        self.__is_3g_cover: bool = is_3g_cover
        self.__is_4g_cover: bool = is_4g_cover

    def __hash__(self):
        return hash((self.__operator, self.__city, self.__is_2g_cover, self.__is_3g_cover, self.__is_4g_cover))

    def __eq__(self, other):
        if not isinstance(other, type(self)): return NotImplemented
        return self.__operator == other.__operator and self.__city == other.__city \
               and self.__is_2g_cover == other.__is_2g_cover and self.__is_3g_cover == self.__is_3g_cover \
               and self.__is_4g_cover == self.__is_4g_cover

    def __repr__(self):
        return str.format("city:{},operator:{},2g_cover:{},3g_cover:{},4g_cover:{}", self.__city, self.__operator,
                          self.__is_2g_cover, self.__is_3g_cover, self.__is_4g_cover)

    @property
    def is_2g_cover(self) -> bool:
        return self.__is_2g_cover

    @property
    def is_2g_cover_int(self) -> int:
        if self.__is_2g_cover:
            return 1
        return 0

    @property
    def is_3g_cover(self) -> bool:
        return self.__is_3g_cover

    @property
    def is_3g_cover_int(self) -> int:
        if self.__is_3g_cover:
            return 1
        return 0

    @property
    def is_4g_cover(self) -> bool:
        return self.__is_4g_cover

    @property
    def is_4g_cover_int(self) -> int:
        if self.__is_3g_cover:
            return 1
        return 0

    @property
    def city(self) -> City:
        return self.__city

    @property
    def operator(self) -> Operator:
        return self.__operator

    @staticmethod
    def header_csv() -> str:
        return ";".join(["Operator", "MNC", "City", "Postcode", "2G", "3G", "4G"])

    @staticmethod
    def header_error_csv() -> str:
        return ";".join(["Operator", "MNC", "Message X", "Y", "2G", "3G", "4G"])

    def render_in_csv(self, delimiter: str = ";") -> str:
        return delimiter.join(
            [self.__operator.name, str(self.__operator.mnc),
             self.__city.name, self.__city.postcode,
             str(self.__is_2g_cover),
             str(self.__is_3g_cover),
             str(self.__is_4g_cover)])


def fuse_operator_coverage_in_one(first: OperatorCoverage, second: OperatorCoverage):
    return OperatorCoverage(operator=first.operator, city=first.city,
                            is_2g_cover=first.is_2g_cover or second.is_2g_cover,
                            is_3g_cover=first.is_3g_cover or second.is_3g_cover,
                            is_4g_cover=first.is_4g_cover or second.is_4g_cover)
