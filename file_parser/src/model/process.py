from typing import Dict, Union, OrderedDict, List
from .model import Operator, OperatorCoverage, get_city_from_lambert_coord, City
from pathlib import Path
from multiprocessing import Process


def create_operator_coverage_from_coverage_data(operator_by_mnc: Dict[int, Operator],
                                                data: Union[Dict[str, str], OrderedDict[str, str]]) -> OperatorCoverage:
    return OperatorCoverage(
        operator=operator_by_mnc[int(data["Operateur"])],
        city=get_city_from_lambert_coord(int(data["X"]), int(data["Y"])),
        is_2g_cover=int(data["2G"]) == 1,
        is_3g_cover=int(data["3G"]) == 1,
        is_4g_cover=int(data["4G"]) == 1
    )


class CsvCreatorProcess(Process):

    def __init__(self, list_data: List[Union[Dict[str, str], OrderedDict[str, str]]],
                 operator_by_mnc: Dict[int, Operator], result_path: str, error_path: str):
        super().__init__()

        self.list_data: List[Union[Dict[str, str], OrderedDict[str, str]]] = list_data
        self.operator_by_mnc: Dict[int, Operator] = operator_by_mnc
        self.result_path: str = result_path
        self.error_path: str = error_path

    def run(self) -> None:
        raw_results = [create_operator_coverage_from_coverage_data(self.operator_by_mnc, data).render_in_csv()
                       for data in self.list_data]

        result_content: List[str] = [result for result in raw_results if "Error with X =" not in result]
        error_content: List[str] = [result for result in raw_results if "Error with X =" in result]

        Path(self.result_path).write_text("\n".join(result_content))
        Path(self.error_path).write_text("\n".join(error_content))
