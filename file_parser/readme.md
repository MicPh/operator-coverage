# File Parser

This directory contains different script to parse data operator coverage from a csv and store it in a database with a specific structure.

## parse.py

This script read two csv files : one with coverage information link to MNC operator and the other with MCC, MCC3 by Operator.

### Result 

And produces two csv files :

* `result.csv` : contains the following data : operator, coverage and city information
* `error.csv` : contains the data which have trouble during the processing (city not found with the precise coordinate)

### Arguments
 
The arguments of the script are :

 * `data_coverage_mnc` : path of the csv file which contains all the coverage information
 * `data_mnc_operator` : path of the csv file with operator information
 * `--number_of_process nb` : option to specify number of process which will be used for the processing (recommended at least 6 because of the slow performance of python in IO process)
 * `--result dir` : option to specify the path of directory where result will be store 
 
 ### Example
 An example of launch for the script can be:
 
 ```bash
 python parser.py ../resources/data_coverage_mnc.csv ../resources/data_mnc_operator.csv --number-of-process 10` 
```

### Limitation

All the result are based on `https://geo.api.gouv.fr/reverse` results so most of the errors are coming from the lacking of result of this API.

It can be a good idea to used multiples sources as MAPS api to get the city name of a location.

The processing are very slow even by using 10 simultaneous process, around 20 minutes 70000 lines to parse.

Maybe it can be a good idea to replace this part with a compile language as go. 

## db_manage.py

This script is used to store the result of `parser.py` in an sqlite3 database.

### Result 

Produce one sqlite3 database file: `database.db` by default.

### Arguments
 
The arguments of the script are :

 * `file` : path for the file used for update
 * `--create` : option to activate the creation of table
 * `--drop` : option to activate the drop of existing
 * `--name-database name` : option to rename the produced database file
 
 ### Example
 An example of launch for the script can be:
 
 ```bash
 python db_manage.py --create --drop ../resources/result.csv 
```